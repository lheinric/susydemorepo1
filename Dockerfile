#start from this image
FROM atlas/analysisbase 
#copy the repo root into /code
COPY . /code
#permanently change workdirectory "\ make sure it's a linebreak"
WORKDIR /code 
RUN source ~/release_setup.sh && \ 
    sudo chown -R atlas /code && \
    g++ -o helloworld helloworld.cxx
    